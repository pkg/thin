require 'gem2deb/rake/spectask'

ipv4 = `ip -4 address | grep inet | wc -l`.strip.to_i
if ipv4 <= 1
  puts "W: just loopback interface or none has IPv4 address, this might be a IPv6 builder. Skipping some tests..."
  pattern = './spec/{controllers/service_spec.rb,
                    server/{swiftiply_spec.rb,tcp_spec.rb,threaded_spec.rb,robustness_spec.rb,stopping_spec.rb,unix_socket_spec.rb},
                    */*_perf_spec.rb}'
else
  pattern = './spec/{controllers/service_spec.rb,*/*_perf_spec.rb}'
end

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = pattern
  spec.verbose = true
end
